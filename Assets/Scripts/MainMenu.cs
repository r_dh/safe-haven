﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	private float _WidthMultiplier = Screen.width / 640;
	private float _HeightMultiplier = Screen.height / 480;
	private bool _HighScore = false;
	private bool _DifficultySelect = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){

		if (!_HighScore && !_DifficultySelect) {
			DrawMainMenu();
				} else if (_HighScore) {
						DrawHighScore ();
				} else if (_DifficultySelect) {
			DrawDifficultySelect();
				}
	}

	void DrawMainMenu()
	{
		GUILayout.BeginArea (new Rect (Screen.width / 2 - (100 * _WidthMultiplier), Screen.height / 2 - (200 * _HeightMultiplier), 200 * _WidthMultiplier, 400 * _HeightMultiplier));
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button ("Start Game")) {
			_DifficultySelect = true;
		}
		if (GUILayout.Button ("High Scores")) {
			_HighScore = true;
		}
		if (GUILayout.Button ("Quit Game")) {
			Application.Quit ();
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndArea ();
	}

	void DrawDifficultySelect()
	{
		GUILayout.BeginArea (new Rect (Screen.width / 2 - (100 * _WidthMultiplier), Screen.height / 2 - (200 * _HeightMultiplier), 200 * _WidthMultiplier, 400 * _HeightMultiplier));
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button ("Easy")) {
			Application.LoadLevel(1);
		}
		if (GUILayout.Button ("Medium")) {
			Application.LoadLevel(2);
		}
		if (GUILayout.Button ("Hard")) {
			Application.LoadLevel(3);
		}
		if (GUILayout.Button ("Return Menu")) {
			_DifficultySelect = false;
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndArea ();
	}

	void DrawHighScore()
	{
		GUILayout.BeginArea (new Rect (Screen.width / 2 - (100 * _WidthMultiplier), Screen.height / 2 - (200 * _HeightMultiplier), 200 * _WidthMultiplier, 400 * _HeightMultiplier));
		GUILayout.FlexibleSpace ();
		GetComponent<HighScore> ().ShowScoreBoard ();
		if (GUILayout.Button ("Return Menu")) {
				_HighScore = false;
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndArea ();
	}
}
