using UnityEngine;
using System.Collections;

public class InGameGUI : MonoBehaviour {
	
	private float _Time = 0.0f;
	
	private const string _ScoreTxt = "Score: ";
	private const string _TimeLeftTxt = "Time played: ";
	private const string _GameOver = "GAME OVER";
	private const string _UnderGameOver = "Press space to start again";
	private const string _Explain1 = "Drag the floating islands left/right with the mouse";
	private const string _EnterNameTxt = "Please enter your name";
	private Font myFont;

	private string _Name = "";

	public int _ModeMultiplier = 1;
	public int _IntesifyTime = 10;

	private bool _GameOverScreen = false;
	private bool _EnterName = false;
	private int _Score = 0;
	private GUIStyle _MyStyle;
	private float _WidthMultiplier = Screen.width / 640;
	private float _HeightMultiplier = Screen.height / 480;

	private bool _Intesified = false;
	
	// Use this for initialization
	void Start () {
		myFont = (Font)Resources.Load("font/Carnevalee Freakshow", typeof(Font));
	}
	
	void Update()
	{
		_Time += Time.deltaTime;
		if (_GameOverScreen && (Input.GetKey (KeyCode.Space)))
						ToggleGameOver ();
				else if (!_GameOverScreen) {
						Intenser();
				}
	}

	void Intenser()
	{
		if(((int)_Time)%_IntesifyTime == 0 && !_Intesified)
		{
			GameObject.Find("Spawner").GetComponent<ShipSpawner>().Intensify();
			GameObject.Find("Spawner").GetComponent<IcebergSpawner>().Intensify();
			_Intesified = true;
		}
		else if(((int)_Time)%_IntesifyTime != 0)
		{
			_Intesified = false;
		}
	}

	void OnGUI()
	{
		_MyStyle = GUI.skin.GetStyle ("label");
		_MyStyle.normal.textColor = Color.black;
		_MyStyle.font = myFont;
		_MyStyle.fontSize = 25;
		
		GUI.Label(new Rect(10, 10, Screen.width, Screen.height), _ScoreTxt + _Score, _MyStyle);
		GUI.Label (new Rect (Screen.width - 280, 10, Screen.width, Screen.height), _TimeLeftTxt + (int)_Time, _MyStyle);
		if (_EnterName)
						DrawName ();
				else if (_GameOverScreen)
						DrawGameOverScreen ();
				else if(_Time < 14.0f){
			if(_Time < 10.0f)				GUI.Label(new Rect(Screen.width/2-400, Screen.height/2 - 400, Screen.width, Screen.height), _Explain1, _MyStyle);
			if(_Time > 5.0f && _Time < 12.0f)GUI.Label(new Rect(Screen.width/2-800, Screen.height/2 - 40, Screen.width, Screen.height), "Clear the path for the tradeships!", _MyStyle);
			if(_Time > 10.0f)				GUI.Label(new Rect(Screen.width/2+200, Screen.height/2 + 60, Screen.width, Screen.height), "Prevent the pirate ships from reaching the end!", _MyStyle);
				}
	}

	void DrawName()
	{
		_MyStyle.normal.textColor = Color.black;
		_MyStyle.font = myFont;
		_MyStyle.fontSize = 35;
		GUI.Label(new Rect(Screen.width/2-200, Screen.height/2-150, Screen.width, Screen.height), _EnterNameTxt, _MyStyle);

		GUILayout.BeginArea (new Rect (Screen.width / 2 - (100 * _WidthMultiplier), Screen.height / 2 - (200 * _HeightMultiplier), 200 * _WidthMultiplier, 400 * _HeightMultiplier));
		GUILayout.FlexibleSpace ();
		_Name = GUILayout.TextField (_Name);
		if (GUILayout.Button ("continue")) {
			ToggleGameOver();
				}
		GUILayout.FlexibleSpace ();
		GUILayout.EndArea ();

	}

	void DrawGameOverScreen ()
	{
		_MyStyle.fontSize = 55;
		GUI.Label(new Rect(Screen.width/2-200, Screen.height/2-200, Screen.width, Screen.height), _GameOver, _MyStyle);
		_MyStyle.fontSize = 20;
		GUI.Label(new Rect(Screen.width/2-200, Screen.height/2-140, Screen.width, Screen.height), _UnderGameOver, _MyStyle);

		//scoreboard
		GUILayout.BeginArea (new Rect (Screen.width / 2 - (100 * _WidthMultiplier), Screen.height / 2 - (150 * _HeightMultiplier), 200 * _WidthMultiplier, 400 * _HeightMultiplier));
		GUILayout.FlexibleSpace ();
		GetComponent<HighScore>().ShowScoreBoard();
		if (GUILayout.Button ("Return Menu")) {
			Application.LoadLevel(0);
			Time.timeScale = 1;
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndArea ();
	}
	
	public void ToggleGameOver()
	{
		if (!_EnterName && !_GameOverScreen) {
						_EnterName = true;
						Time.timeScale = 0;
				}
		else if(_EnterName && !_GameOverScreen){
			_EnterName = false;
			_GameOverScreen = true;
			GetComponent<HighScore>().CheckAndChangeScore(_Name, ((_Score + (int)_Time*4)* _ModeMultiplier) );
		}
		else {
			_GameOverScreen = false;
			Time.timeScale = 1;
			Application.LoadLevel(Application.loadedLevel);
		}
	}
	
	public void AddScore(int amount)
	{
		_Score += amount;
	}
	
	public float GetScore()
	{
		return _Score;
	}
}