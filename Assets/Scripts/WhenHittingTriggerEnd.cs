﻿using UnityEngine;
using System.Collections;

public class WhenHittingTriggerEnd : MonoBehaviour {
	
	public int _Score = 10;
	public string _ObjectSerial = "Ship";

	
	
	// Use this for initialization
	void Start () {

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "TriggerEnd") {
			
			if(_ObjectSerial == "Ship")
			{
				Camera.main.GetComponent<Sounds>().PlaySound("money"); //Play sound
				GameObject.Destroy (gameObject);
				Camera.main.GetComponent<InGameGUI>().AddScore(_Score);
			}
			else if(_ObjectSerial == "Pirate")
			{
				Camera.main.GetComponent<Sounds>().PlaySound("crashBig"); //Play sound
				GameObject marker = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
				marker.transform.position = new Vector3(transform.position.x, transform.position.y - 10.0f, transform.position.z);
				marker.transform.localScale += new Vector3(30.0f, 0.0f, 30.0f);
				marker.renderer.material.color = Color.red;
				Camera.main.GetComponent<InGameGUI>().ToggleGameOver();
			}
		}
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Iceberg") {
			if(_ObjectSerial == "Ship")
			{
				Camera.main.GetComponent<Sounds>().PlaySound("crashBig"); //Play sound
				GameObject marker = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
				marker.transform.position = new Vector3(transform.position.x, transform.position.y - 10.0f, transform.position.z);
				marker.transform.localScale += new Vector3(30.0f, 0.0f, 30.0f);
				marker.renderer.material.color = Color.red;

				Camera.main.GetComponent<InGameGUI>().ToggleGameOver();
              //  Debug.Log("Iceberg collided with ship");
			}
			else if(_ObjectSerial == "Pirate")
			{
				Camera.main.GetComponent<Sounds>().PlaySound("money"); //Play sound
				Camera.main.GetComponent<InGameGUI>().AddScore(_Score);
				GameObject.Destroy (gameObject);
			}
		}
	}

	public void SetObjectSerial(string Serial)
	{
		_ObjectSerial = Serial;
	}
}