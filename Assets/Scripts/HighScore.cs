﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class HighScore : MonoBehaviour {

	// Use this for initialization
	void Start () {
		ReadHighScore();
	}

	void ReadHighScore()
	{
		StreamReader tr = new StreamReader(Application.dataPath + "/Resources/HighScore.txt");
		string line = "";
		int player = 0;
		while ( (line = tr.ReadLine()) != null )
		{
			PlayerPrefs.SetString("player" + player,line.Substring(0,line.IndexOf(';')));
			PlayerPrefs.SetInt("" + player, int.Parse(line.Substring(line.IndexOf(';') + 1)));
			++player;
		}
		
		tr.Close();
	}

	void OnDestroy()
	{
		StreamWriter stWr = new StreamWriter (Application.dataPath + "/Resources/HighScore.txt");
		string line = "";
		for(int player = 0; player < 10; ++player)
		{
			line += PlayerPrefs.GetString("player" + player);
			line += ";";
			line += PlayerPrefs.GetInt("" + player);
			stWr.WriteLine(line);
			line = "";
		}
		
		stWr.Close ();
	}

	// Update is called once per frame
	public void ShowScoreBoard()
	{
		GUILayout.BeginVertical ();
		GUILayout.BeginHorizontal();
		GUILayout.Box("player");
		GUILayout.Box("name");
		GUILayout.Box("Score");
		GUILayout.EndHorizontal();
		for(int player = 0; player < 10; ++player)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Box("" + (player + 1));
			GUILayout.Box(PlayerPrefs.GetString("player" + player));
			GUILayout.Box("" + PlayerPrefs.GetInt("" + player));
			GUILayout.EndHorizontal();
		}
		GUILayout.EndVertical ();
	}

	public void CheckAndChangeScore(string name, int score)
	{
		string helpName = "", helpName2 = name;
		int helpScore = 0, helpScore2 = score;
		for (int playerlist = 0; playerlist < 10; ++playerlist) {
			if(helpScore2 > PlayerPrefs.GetInt("" + playerlist))
			{
				helpName = PlayerPrefs.GetString("player" + playerlist);
				PlayerPrefs.SetString("player" + playerlist, helpName2);
				helpScore = PlayerPrefs.GetInt("" + playerlist);
				PlayerPrefs.SetInt("" + playerlist, helpScore2);
				helpName2 = helpName;
				helpScore2 = helpScore;
			}
		}
	}
}
