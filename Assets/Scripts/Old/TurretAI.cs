﻿using UnityEngine;
using System.Collections;

public class TurretAI : MonoBehaviour {


	public Vector3  playerPos,	//Player position
	turretPos;					//Turret (own) position
	public GameObject bullet,	//Bullet prefab, assign in inspector
	bulletSpawn;				//BulletSpawn empty gameobject, assign in inspector
	GameObject player;			//Player gameobject
	float turretSpeed = 1.0f,	//Rotation speed of the turret
	viewRange = 10.0f,			//Maximum attack range of turret
	timer = 0.0f;				//Interval between shots

	
	void Start () {
		player = GameObject.FindWithTag ("Player");
		turretPos = transform.position;
	}

	void Update () {
		playerPos = player.transform.position;
		if (Vector3.Distance (turretPos, playerPos) <= viewRange) {
			Quaternion rotate = Quaternion.LookRotation(playerPos - turretPos);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotate, Time.deltaTime * turretSpeed);
	
			timer += Time.deltaTime;
			if(timer >= 3.0f){
				timer = 0.0f;

				GameObject go;
				go = Instantiate(bullet, bulletSpawn.transform.position, bulletSpawn.transform.rotation) as GameObject;//Rigidbody
				Rigidbody clone = go.GetComponent<Rigidbody>();

				clone.AddForce((playerPos - bulletSpawn.transform.position) * 40);
			}
		}

		SetMaxRotation ();
	}

	//max X rotation: 12 (beneden) -33 (boven)
	//max y rotation: unlimited
	//max z rotation: NULL
	void SetMaxRotation(){

		if (transform.eulerAngles.x > 12 && transform.eulerAngles.x < 320) {
						float lowerBound = transform.eulerAngles.x - 12;
						float upperBound = 320 - transform.eulerAngles.x;

						if(lowerBound < upperBound)	transform.Rotate (new Vector3 (-lowerBound, 0, 0));
						else transform.Rotate (new Vector3 (upperBound, 0, 0));
		}
		if (transform.rotation.z != 0)transform.Rotate(new Vector3 (0, 0, -transform.eulerAngles.z));
	}
}
