﻿using UnityEngine;
using System.Collections;

public class Sounds : MonoBehaviour {
	//ijsbergen spawnen, drijven, verslepen, botsen met elkaar, botsen met schepen
	//schepen spawnen, varen, botsen, einde halen
	//piraten spawnen, varen, botsen, einde halen


	private AudioSource au_Ambient, au_Ambient2,au_Crash1, au_Crash2, au_Crash3, au_CrashBig, au_Money, au_Clash;

	// Use this for initialization
	void Start () {
		//Ambient example
		au_Ambient = (AudioSource)gameObject.AddComponent ("AudioSource");
		au_Ambient2 = (AudioSource)gameObject.AddComponent ("AudioSource");

		AudioClip ambientSound;
		AudioClip ambientSound2;

		ambientSound = (AudioClip)Resources.Load ("audio/ambient");
		ambientSound2 = (AudioClip)Resources.Load ("audio/ambient2");

		au_Ambient.clip = ambientSound;
		au_Ambient2.clip = ambientSound2;

		int i = Random.Range (0, 1);
		if (i == 1) {
						au_Ambient.loop = true;
						au_Ambient.Play ();
				} else {
			au_Ambient2.loop = true;
			au_Ambient2.Play ();
				}
	
		//Other sounds
		au_Crash1 = (AudioSource)gameObject.AddComponent ("AudioSource");
		au_Crash2 = (AudioSource)gameObject.AddComponent ("AudioSource");
		au_Crash3 = (AudioSource)gameObject.AddComponent ("AudioSource");
		au_CrashBig = (AudioSource)gameObject.AddComponent ("AudioSource");
		au_Money = (AudioSource)gameObject.AddComponent ("AudioSource");
		au_Clash = (AudioSource)gameObject.AddComponent ("AudioSource");

		//Corresponding audioclips
		AudioClip crash1Sound = (AudioClip)Resources.Load ("audio/blast1");
		AudioClip crash2Sound = (AudioClip)Resources.Load ("audio/blast2");
		AudioClip crash3Sound = (AudioClip)Resources.Load ("audio/blast3");
		AudioClip crashBigSound = (AudioClip)Resources.Load ("audio/blastBig");
		AudioClip moneySound = (AudioClip)Resources.Load ("audio/plunder"); 
		AudioClip clashSound = (AudioClip)Resources.Load ("audio/upgrade");

		//Match
		au_Crash1.clip = crash1Sound;
		au_Crash2.clip = crash2Sound;
		au_Crash2.volume = 1;
		au_Crash3.clip = crash3Sound;
		au_CrashBig.clip = crashBigSound;
		au_Money.clip = moneySound;
		au_Clash.clip = clashSound;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void PlaySound(string soundName){ //Name of audiosource: crash1 - 3, crashBig, money
		switch (soundName) {
		case "crash1": au_Crash1.Play();
			break;
		case "crash2": au_Crash2.Play();
			break;
		case "crash3": au_Crash3.Play();
			break;
		case "crashBig": au_CrashBig.Play();
			break;
		case "money": au_Money.Play();
			break;
		case "clash": au_Clash.Play();
			break;
				}
		}
	public void StopAmbient(){
		au_Ambient.Stop ();
		au_Ambient2.Stop ();
		}
}
