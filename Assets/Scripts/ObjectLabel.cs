﻿using UnityEngine;
using System.Collections;

public class ObjectLabel : MonoBehaviour {

	//private float _Time = 0.0f;
	private GUIStyle _MyStyle;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	//	_Time += Time.deltaTime;
	}

	void OnGUI(){
		_MyStyle = GUI.skin.GetStyle ("label");
		_MyStyle.normal.textColor = Color.black;
		_MyStyle.fontSize = 11;

	//	if (_Time < 10.0f) {
			//Camera.main.WorldToScreenPoint(GameObject.Find("UnityIceberg(Clone)").GetComponent<Transform>().position); 
			GUI.Label(new Rect(Camera.main.WorldToScreenPoint(gameObject.GetComponent<Transform>().position).x - 20,
			                   Screen.height - Camera.main.WorldToScreenPoint(gameObject.GetComponent<Transform>().position).y + 25,
			                   Screen.width, Screen.height), gameObject.tag, _MyStyle);
	//	}
		}
}
