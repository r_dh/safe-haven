﻿using UnityEngine;
using System.Collections;

public class IcebergSpawner : MonoBehaviour {
   
    public GameObject iceBergPrefab;
	//Rigidbody iceBergPrefabPrivate; //Rigidbody
    float startPos = 100, //-100 on z axis
    maxPos = 100, //100 on x axis
	minPos = -150, //x axis
	nextInterval, //next interval set to spawn
	timer = 0;
    public float minInterval = 2.0f, //min time between spawns 
	maxInterval = 6.0f; //max time between spawns
    
	public string _Mode = "medium";

	void Start () {//TestPrefab = (GameObject)Resources.Load("MyPrefab", typeof(GameObject));
		//iceBergPrefabPrivate = (Rigidbody)Resources.Load("UnityIceberg"); //, typeof(GameObject) //GameObject ipv Rigidbody x2
        nextInterval = Random.Range(minInterval, maxInterval);
    }
    
    void Update () {
        timer += Time.deltaTime;
		if (Mathf.Abs(timer - nextInterval) < 0.05f || timer > nextInterval) {
            timer = 0;
            nextInterval = Random.Range (minInterval, maxInterval);
            GameObject icebergInstance;
			Vector3 pos = new Vector3 (Random.Range (minPos, maxPos), 0, startPos*2);
            Quaternion rot = transform.rotation; //Quaternion.Euler(0, Random.Range (-180, 180), 0);
			//Rigidbody instance = Instantiate(Resources.Load("UnityIceberg", typeof(GameObject)), pos , rot) as Rigidbody;
			icebergInstance =  Instantiate (iceBergPrefab, pos , rot) as GameObject;
			icebergInstance.tag = "Island";
        }
    }

	public void Intensify()
	{
		if (maxInterval > 2.11f) 
		{
			maxInterval -= 0.1f;
		}
	}

	public float GetMaxPos()
	{
		return maxPos;
	}

	public float GetMinPos()
	{
		return minPos;
	}
}
