﻿using UnityEngine;
using System.Collections;

public class Ship_Regular : MonoBehaviour {

	Transform _Ship;

	public float _MaxSpeed = 10.0f;
	public float _MinSpeed = 4.0f;

	float speedX = 50.0f,
	counter = 0.0f;
	// Use this for initialization
	void Start () {
		speedX = Random.Range (_MinSpeed, _MaxSpeed);
		_Ship = gameObject.GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		counter += 0.05f;
		_Ship.Translate(new Vector3(speedX * Time.deltaTime, Mathf.Sin(counter)* Time.deltaTime, 0));
	}
	
	public void Move(float x, float y){
		_Ship.Translate(new Vector3(x, y, 0));
	}
}
