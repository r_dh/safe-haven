﻿using UnityEngine;
using System.Collections;

public class Float_Iceberg : MonoBehaviour {
	
	
	Transform _Iceberg;
	public float _SpeedMin = 4.0f;
	public float _SpeedMax = 20.0f;
	
	private float _Speed = 0.0f;
	private float	counter = 0.0f,
	collisionCounter = 0.0f,
	scaleFactor = 0.5f;
	
	// Use this for initialization
	void Start () {
		_Speed = Random.Range (-_SpeedMin, -_SpeedMax);
		_Iceberg = gameObject.GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		counter += 0.05f;
		_Iceberg.Translate(new Vector3( 0, -_Speed * Time.deltaTime, Mathf.Sin(counter)* Time.deltaTime));
	}
	
	public void Move(float x, float y){
		_Iceberg.Translate(new Vector3(x, y, 0));
	}
	
	void OnCollisionEnter(Collision other){
		
		//collision between 2 objects
		if (other.gameObject.tag == "Iceberg"){
			//size and rescaling

			Camera.main.GetComponent<Sounds>().PlaySound("crash1"); //Play sound
			if(other.gameObject.transform.localScale.x < gameObject.transform.localScale.x)
			{
				IceBergReform(other);
			}
			else if(other.gameObject.transform.position.z > gameObject.transform.position.z &&
			        other.gameObject.transform.localScale.x == gameObject.transform.localScale.x)
			{
				IceBergReform(other);
			}
			
			
			//collsion between a ship and a iceberg 
		} else if (other.gameObject.tag == "Pirate" && collisionCounter > 1.1f){
			Camera.main.GetComponent<Sounds>().PlaySound("crash2"); //Play sound
			--collisionCounter;
			transform.localScale -= new Vector3(scaleFactor / collisionCounter, scaleFactor / collisionCounter, scaleFactor / collisionCounter);
		}
	}
	
	void IceBergReform(Collision other)
	{
		float col = collisionCounter;
		collisionCounter += other.gameObject.GetComponent<Float_Iceberg> ().GetCollisionCounter ()+1;
		for(float i = 1; i < collisionCounter + 1; ++i){
			gameObject.transform.localScale += new Vector3 (scaleFactor / i,scaleFactor / i,scaleFactor / i);
		}
		//avg speed and position
		_Speed = (other.gameObject.GetComponent<Float_Iceberg> ().GetSpeed ()*(other.gameObject.GetComponent<Float_Iceberg> ().GetCollisionCounter () + 1)  + _Speed*col) / collisionCounter;
		_Iceberg.position = new Vector3 (_Iceberg.position.x, _Iceberg.position.y, (_Iceberg.position.z + other.gameObject.GetComponent<Float_Iceberg> ().GetPosZ ()) / 2);
		
		//destroy the above iceberg
		GameObject.Destroy (other.gameObject);
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "TriggerEndBerg") {
			Destroy(gameObject);
		}
	}
	
	public float GetSpeed()
	{
		return _Speed;
	}
	
	public float GetPosZ()
	{
		return _Iceberg.position.z;
	}
	
	public float GetCollisionCounter()
	{
		return collisionCounter;
	}
}
