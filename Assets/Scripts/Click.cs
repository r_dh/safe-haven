﻿using UnityEngine;
using System.Collections;

public class Click : MonoBehaviour {
	
	private Transform _Object; //The object we will move
	private RaycastHit _Hit;
	private Ray _Ray;
	Vector3 startPosClick, endPosClick, offset;

	private bool _Init = false;
	private float _MaxBoundary;
	private float _MinBoundary;
	
	void Start()
	{
	}

	public void Initiate()
	{
		_MaxBoundary = GameObject.Find("Spawner").GetComponent<IcebergSpawner>().GetMaxPos();
		_MinBoundary = GameObject.Find("Spawner").GetComponent<IcebergSpawner>().GetMinPos();
	}

	void Update ()
	{
		_Ray = camera.ScreenPointToRay(Input.mousePosition); //Gets the mouse position in the form of a ray
		//Debug.Log (_Ray); //_Ray DOES WORK, ignore unity error
	if (Input.GetButtonDown("Fire1")) { 
		
		if (!_Object) {
			if (Physics.Raycast(_Ray, out _Hit)) { //Then see if an object is beneath us using raycasting
				if(_Hit.transform.tag == "Iceberg")
				{
					_Object = _Hit.transform; //If we hit an object then hold on to the object
					startPosClick = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width - Input.mousePosition.x, Screen.height - Input.mousePosition.y, Camera.main.transform.position.z - 200f));
				}
			}
		}
	}
	
	else if (Input.GetButtonUp("Fire1")) {
		_Object = null; //Let go of the object
	}
	
	if (_Object) {
	//	Debug.Log("object dragging");
			endPosClick = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y, Camera.main.transform.position.z - 200f));
			if(startPosClick.x > endPosClick.x) {
				offset.x = -Mathf.Abs(Mathf.Abs(startPosClick.x) - Mathf.Abs(endPosClick.x));
			}
			else{
				offset.x = Mathf.Abs(Mathf.Abs(endPosClick.x) - Mathf.Abs(startPosClick.x));
			}

			_Object.transform.position = new Vector3(_Object.position.x + offset.x, _Object.position.y, _Object.position.z);



			if(!_Init)
			{
				Initiate();
				_Init = true;
			}

			if(_Object.transform.position.x > _MaxBoundary)
			{
				_Object.transform.position = new Vector3(_MaxBoundary, _Object.position.y, _Object.position.z);
			}
			else if(_Object.transform.position.x < _MinBoundary)
			{
				_Object.transform.position = new Vector3(_MinBoundary, _Object.position.y, _Object.position.z);
			}
		
			startPosClick = endPosClick;
			}
    }
}
