﻿using UnityEngine;
using System.Collections;

public class ShipSpawner : MonoBehaviour {

	public GameObject[] shipPrefabs = new GameObject[6];
	float startPos = -100, //-100 on x axis
	maxPos = 60, //100 on z axis
	minPos = -100, //z axis
	nextInterval, //next interval set to spawn
	timer = 0,
	_Time = 0;
	public float piratePercentage = 0.2f,//0.1f is 10%
	_MediumPercentage = 0.2f,
	_HardPercentage = 0.2f,
	minInterval = 2f, //min time between spawns 
	maxInterval = 6f; //max time between spawns

	public string _Mode = "medium";

	void Start () {
		nextInterval = Random.Range(minInterval, maxInterval);
	}

	void Update () {
		timer += Time.deltaTime;
		_Time += Time.deltaTime;
		if (Mathf.Abs(timer - nextInterval) < 0.05f || timer > nextInterval) {
			timer = 0;
			nextInterval = Random.Range (minInterval, maxInterval);
			GameObject shipInstance;
			Vector3 pos = new Vector3 (startPos*2, 0, Random.Range (minPos, maxPos));

			if (Random.Range(0.0f, 1.0f) <= piratePercentage && _Time > 15.0f)
			{
				if(_Mode == "easy")
				{
					shipInstance = 	Instantiate (shipPrefabs[3], pos , transform.rotation) as GameObject; //pos 1 is pirateship
					shipInstance.GetComponent<WhenHittingTriggerEnd>().SetObjectSerial("Pirate");
				}
				else if(_Mode == "medium")
				{
					shipInstance = 	Instantiate (shipPrefabs[4], pos , transform.rotation) as GameObject; //pos 1 is pirateship
					shipInstance.GetComponent<WhenHittingTriggerEnd>().SetObjectSerial("Pirate");
				}
				else if(_Mode == "hard")
				{
					shipInstance = 	Instantiate (shipPrefabs[5], pos , transform.rotation) as GameObject; //pos 1 is pirateship
					shipInstance.GetComponent<WhenHittingTriggerEnd>().SetObjectSerial("Pirate");
				}
				//shipInstance.transform.Find("Box002").renderer.material.color = Color.black;
			}
			else {
				if(_Mode == "easy")
				{
					shipInstance = 	Instantiate (shipPrefabs[0], pos , transform.rotation) as GameObject; // pos 0 is gwn ship
					shipInstance.tag = "Ship";
				}
				else if(_Mode == "medium")
				{
					if (Random.Range(0.0f, 1.0f) <= _MediumPercentage){
						shipInstance = 	Instantiate (shipPrefabs[1], pos , transform.rotation) as GameObject; // pos 1 is gwn ship
						shipInstance.tag = "Ship";
					}
					else
					{
						shipInstance = 	Instantiate (shipPrefabs[0], pos , transform.rotation) as GameObject; // pos 0 is gwn ship
						shipInstance.tag = "Ship";
					}
				}
				else if(_Mode == "hard")
				{
					if (Random.Range(0.0f, 1.0f) <= _HardPercentage){
						shipInstance = 	Instantiate (shipPrefabs[2], pos , transform.rotation) as GameObject; // pos 1 is gwn ship
						shipInstance.tag = "Ship";
					}
					else if (Random.Range(0.0f, 1.0f) <= _MediumPercentage){
						shipInstance = 	Instantiate (shipPrefabs[1], pos , transform.rotation) as GameObject; // pos 1 is gwn ship
						shipInstance.tag = "Ship";
					}
					else
					{
						shipInstance = 	Instantiate (shipPrefabs[0], pos , transform.rotation) as GameObject; // pos 0 is gwn ship
						shipInstance.tag = "Ship";
					}
				}
			}
		}
	}

	public void Intensify()
	{
		//Debug.Log ("Inteser");
		if (_HardPercentage < 0.5f) {
			_HardPercentage += 0.01f;
		}
		if (_MediumPercentage < 0.7f) {
			_MediumPercentage += 0.01f;
		}
		if (maxInterval > 2.11f) {
			maxInterval -= 0.1f;
		}
	}
}
